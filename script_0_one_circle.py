import tensorflow as tf
import numpy as np
import argparse
tf.logging.set_verbosity(tf.logging.INFO)

def generate_data(args,mode):
	
	upper_limit = 12
	lower_limit = 0
	
	if mode == 'train':
		train_features = lower_limit + np.random.rand(args.num_train_data,2) * (upper_limit - lower_limit)
		train_labels = np.zeros([args.num_train_data,2])
		for i in range(args.num_train_data):
			for j in range(len(args.radius)):
				if np.sqrt((train_features[i,0] - args.centre_x[j])**2 + (train_features[i,1] - args.centre_y[j])**2) < args.radius[j] :
					train_labels[i,1] = 1
				else :
					train_labels[i,0] = 1
		return train_features,train_labels
	eval_features = lower_limit + np.random.rand(args.num_eval_data,2) * (upper_limit - lower_limit)
	eval_labels = np.zeros([args.num_eval_data,2])
	for i in range(args.num_eval_data):
		for j in range(len(args.radius)):
			if np.sqrt((eval_features[i,0] - args.centre_x[j])**2 + (eval_features[i,1] - args.centre_y[j])**2) < args.radius[j] :
				eval_labels[i,1] = 1
			else:
				eval_labels[i,0] = 1
	return eval_features,eval_labels

def get_train_input_fn(args):
	features,labels = generate_data(args,mode = 'train')
	print(features.shape)
	print(labels.shape)
	def train_input_fn():
		dataset = tf.data.Dataset.from_tensor_slices((features,labels))
		dataset = dataset.repeat(args.num_epochs)
		dataset = dataset.batch(args.batchsize)
		iterator = dataset.make_one_shot_iterator()
		return iterator.get_next()
		#return dataset
	return train_input_fn

def get_eval_input_fn(args):
	features,labels = generate_data(args,mode = 'eval')
	def eval_input_fn():
		dataset = tf.data.Dataset.from_tensor_slices((features,labels))

		# For estimator function
		# dataset = dataset.batch(args.num_eval_data)
		
		# For non-estimator function
		dataset = dataset.repeat(args.train_steps // args.steps_between_eval)
		dataset = dataset.batch(args.train_steps // args.steps_between_eval)

		iterator = dataset.make_one_shot_iterator()
		return iterator.get_next()
	return eval_input_fn

def architecture(features,args):
	n_input = features.shape[1]
	layer = tf.layers.dense(inputs = features,
							kernel_initializer = tf.initializers.random_normal(stddev=np.sqrt(1/int(n_input))),
                            bias_initializer = tf.initializers.random_normal(),
                            units=args.n_hidden[0],activation = tf.nn.relu,name = 'layer_1')
	for i in range(1,args.n_layers):
		layer = tf.layers.dense(inputs = layer, units=args.n_hidden[i],
							kernel_initializer = tf.initializers.random_normal(stddev=np.sqrt(1/int(n_input))),
                            bias_initializer = tf.initializers.random_normal(),
                            activation = tf.nn.relu,name = 'layer_'+str(i+1))
	layer = tf.layers.dense(inputs = layer, units = args.num_classes,activation = tf.nn.softmax,name = 'layer_' + str(args.n_layers+1))
	return layer

def model_function(features,labels,mode,params):

	optimizer = tf.train.GradientDescentOptimizer(learning_rate = args.learning_rate)
	predictions = architecture(features = features,args = args)
	loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels = labels, logits = predictions))
	accuracy = tf.metrics.accuracy(labels = labels, predictions = tf.round(predictions))
	train_op = optimizer.minimize(loss = loss, global_step = tf.train.get_global_step())
	eval_op = {'accuracy':accuracy}
	return tf.estimator.EstimatorSpec(mode = mode, loss = loss,train_op = train_op, predictions = predictions,
										eval_metric_ops = eval_op )

def estimator_function(args):

	run_config = tf.estimator.RunConfig(model_dir = args.model_dir, save_checkpoints_steps = args.steps_between_eval)
	classifier = tf.estimator.Estimator(model_fn = model_function, config = run_config, params = args)
	train_spec = tf.estimator.TrainSpec(get_train_input_fn(args), max_steps = args.train_steps) 
	eval_spec = tf.estimator.EvalSpec(get_eval_input_fn(args))
	result = tf.estimator.train_and_evaluate(classifier,train_spec,eval_spec)
	print(result)
	return None

def test_input_function(args):
	input_fn = get_train_input_fn(args)
	input_data = input_fn() 
	i = 0
	with tf.Session() as sess:
		sess.run(tf.global_variables_initializer())
		while True :
			try :
				A = sess.run(input_data)
				print(A[0].shape)
				print(A[1].shape)
				i = i + 1
			except :
				print(i)
				break


def non_estimator_function(args):

	train_input_fn = get_train_input_fn(args)
	train_data = train_input_fn()
	eval_input_fn = get_eval_input_fn(args)
	eval_data = eval_input_fn()
	X = tf.placeholder(tf.float32,shape = [None,2])
	Y = tf.placeholder(tf.float32,shape = [None,2])
	predictions = architecture(X,args)
	loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels = Y, logits = predictions))
	accuracy = tf.metrics.accuracy(labels = Y, predictions = tf.round(predictions))
	optimizer = tf.train.GradientDescentOptimizer(learning_rate = args.learning_rate)
	train_op = optimizer.minimize(loss = loss, global_step = tf.train.get_global_step())
	with tf.Session() as sess:
		sess.run(tf.global_variables_initializer())
		sess.run(tf.local_variables_initializer())
		Train_Acc = 0
		Train_Loss = 0
		Eval_Acc = 0
		Eval_Loss = 0
		num_steps = args.train_steps // args.steps_between_eval
		for i in range(num_steps):
			temp_train_loss = 0
			temp_eval_loss = 0
			temp_train_acc = 0
			temp_eval_acc = 0
			for j in range(args.steps_between_eval):
				train_features,train_labels = sess.run(train_data)
				_,temp1,temp2 = sess.run([train_op,accuracy,loss], feed_dict={X : train_features, Y : train_labels})
				temp_train_acc = temp1[0]
				temp_train_loss = temp2
			eval_features,eval_labels = sess.run(eval_data)
			temp1,temp2 = sess.run([accuracy,loss], feed_dict={X : eval_features, Y : eval_labels})
			temp_eval_acc = temp1[0]
			temp_eval_loss = temp2
			print("Step number "+str(i)+ " -- train_accuracy : "+str(temp_train_acc) + " -- train_loss : " 
				+ str(temp_train_loss) + " -- eval_accuracy : "+str(temp_eval_acc) 
				+ " -- eval_loss : " + str(temp_eval_loss))
			
	return None



if __name__ == '__main__':
	
	parser = argparse.ArgumentParser()
	parser.add_argument('--num_train_data', type = int, default = 100000)
	parser.add_argument('--num_eval_data', type = int, default = 8000)
	parser.add_argument('--num_epochs', type = int, default = 10000)
	parser.add_argument('--steps_between_eval', type = int, default = 10000)
	parser.add_argument('--train_steps', type = int, default = 10000000)
	parser.add_argument('--model_dir', type = str, default = './models/model_1')
	parser.add_argument('--learning_rate', type = float, default = 0.001)
	parser.add_argument('--n_hidden',nargs='+', default = [16,8,8,4])
	parser.add_argument('--n_layers', type = int, default = 4)
	parser.add_argument('--num_classes', type = int, default = 2)
	parser.add_argument('--batchsize', type = int, default = 100)
	parser.add_argument('--centre_x',nargs = '+', default = [3,3,9,9])
	parser.add_argument('--centre_y',nargs = '+', default = [3,9,3,9])
	parser.add_argument('--radius',nargs = '+', default = [2,2,2,2])
	args = parser.parse_args()
	# estimator_function(args)
	non_estimator_function(args)
	



